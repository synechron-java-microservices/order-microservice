package com.synechron.springbootdemo.config;

import org.springframework.stereotype.Component;

@Component
public class ServiceConfiguration {

    private String customMessage;

    public String getCustomMessage() {
        return this.customMessage;
    }
}