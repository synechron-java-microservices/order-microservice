package com.synechron.springbootdemo.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserContext {

    public static final String CORRELATION_ID = "CORRELATION_ID";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";

    private String correlationId;

    private String authToken;
}