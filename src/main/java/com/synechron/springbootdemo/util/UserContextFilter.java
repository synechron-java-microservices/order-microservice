package com.synechron.springbootdemo.util;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class UserContextFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        final String correlationId = httpServletRequest.getHeader(UserContext.CORRELATION_ID);
        final String authToken = httpServletRequest.getHeader(UserContext.AUTH_TOKEN);

        //set the correlation Id in the UserContextHolder
        UserContextHolder.getContext().setCorrelationId(correlationId);
        UserContextHolder.getContext().setAuthToken(authToken);
        chain.doFilter(request, response);
    }
}