package com.synechron.springbootdemo.service;

import com.synechron.springbootdemo.dao.OrderRepository;
import com.synechron.springbootdemo.model.MoneyTransfer;
import com.synechron.springbootdemo.model.Order;
import com.synechron.springbootdemo.util.UserContextHolder;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderDAO;

    private final RestTemplate restTemplate;

    //1 way using Discovery client
    private final DiscoveryClient discoveryClient;

    private final InventoryServiceFeignClient inventoryServiceFeignClient;

    @Override
    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallbackOrder")
    @Retry(name = "retryService", fallbackMethod = "fallbackOrder")
    public Order saveOrder(Order order) {
        log.info("Came inside the fetchInventory method :::: ");
        MoneyTransfer moneyTransfer = MoneyTransfer.builder()
                                            .from(order.getCustomerName())
                                            .to(order.getMerchantName())
                                            .money(order.getTotalPrice())
                                            .notes(order.getNotes())
                                            .build();
        log.info("Fetching the correlation id from the HttpServletRequest {} and auth token {} ::", UserContextHolder.getContext().getCorrelationId(), UserContextHolder.getContext().getAuthToken());

        ResponseEntity<MoneyTransfer> responseFromPaymentService = this.restTemplate.postForEntity("http://PAYMENTSERVICE/api/v1/payments", moneyTransfer, MoneyTransfer.class);
        final ResponseEntity<Integer> updatedQty = this.restTemplate.postForEntity("http://INVENTORYSERVICE/api/v1/inventory", 1, Integer.class);

        //final ResponseEntity<Integer> responseEntity = this.inventoryServiceFeignClient.updateQty();

        //log.info(" Updated inventory from the inventory service is {}", responseEntity.getBody());
        /*final List<ServiceInstance> instances = this.discoveryClient.getInstances("INVENTORYSERVICE");
        if (! instances.isEmpty() ){
            final ServiceInstance serviceInstance = instances.get(0);
            final String serviceUri = serviceInstance.getUri().toString();
            log.info(" Number of service instances {}", instances.size());
            log.info(" Executing the post call to inventory service :::: {}", serviceUri);
            final ResponseEntity<Integer> updatedQty = this.restTemplate.postForEntity(serviceUri + "/api/v1/inventory", Integer.class, Integer.class);
            log.info(" Updated inventory from the inventory service is {} ", updatedQty.getBody());
        }*/
        return this.orderDAO.save(order);
    }

    private Order fallbackOrder(Throwable throwable){
        log.error("Exception while updating the inventory:: Resorting to fallback ");
        log.error("Error message : {}", throwable.getMessage());
        return Order.builder().orderDate(LocalDate.now()).orderId(1111).totalPrice(20000).build();
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderDAO.findAll());
    }


    @Override
    public Order fetchOrderByOrderId(long orderId) {
        return this.orderDAO.findById(orderId)
                .orElseThrow(OrderServiceImpl::invalidOrderId);
    }

    private static IllegalArgumentException invalidOrderId() {
        return new IllegalArgumentException("Invalid Order Id");
    }

    @Override
    public void archiveOrderByOrderId(long orderId) {
        this.orderDAO.deleteById(orderId);
    }

}